export default {
    namespaced : true,
    state : {
        status : false,
        component : '',
        params : {}
    },
    getters : {
        status : function(state){return state.status},
        component : function(state){return state.component},
        params : function(state){return state.params}
    },
    mutations : {
        setStatus : function(state,status){            
                state.status = status
                console.log(state.status)                   
            },
        setComponent : function(state,{component,params}){            
                state.component = component, 
                state.params = params 
                console.log(state.component)              
            },
    },
    actions : {
        setStatus : function({commit},status){
                commit('setStatus',status)
                },
        setComponent : function({commit},{component,params}){
            console.log("masuk dialog.js")
                commit('setComponent',{component,params})
                commit('setStatus',true)
                },
    }
}